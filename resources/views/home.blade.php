@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Queue</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        {{ Auth::user()->name }}
                        {{ Auth::user()->id }}
                    </div>
                        <hr>
                        <hr>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <hr>
                        <hr>
                    <div>
                       @if ((count($users)) > 0)
                           @foreach ($users as $user)
                           <a href="" class="boards__item">
                               <span class="boards__name">{{ $user->id }}</span>
                           </a>
                           @endforeach
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
