<?php

namespace App\Http\Controllers;

class AjaxController extends Controller
{

    const DEFAULT_RENDERER = "#content";
    const SUCCESS_STATUS = 200;
    const ERROR_STATUS = 201;

    const RESPONSE_TYPE_JSON = 'application/json';

    public $response = [];

    public function setStatus($status = 200) {
        $this->response['status'] = $status;
    }

    public function getStatus() {
        if(empty($this->response['status'])) {
            $this->setStatus();
        }
    }

    public function setMessage($message) {
        $this->response['message'] = $message;
    }

    public function addRenderer($renderer = self::DEFAULT_RENDERER) {
        $this->response['renderers'][] = $renderer;
    }

    public function setRedirect($redirectUrl) {
        $this->response['redirect'] = $redirectUrl;
    }

    public function setContent($content) {
        $this->response['content'] = $content;
    }

    public function getResponseJson($type = self::RESPONSE_TYPE_JSON) {
        $this->validateResponse();
        $this->getStatus();
        return response(json_encode($this->response))
            ->header('Content-Type', $type);
    }

    private function validateResponse()
    {
        if (empty($this->response['redirect']) && !is_array($this->response['renderers'])) {
            throw new \Exception("Renderers not found.");
        }
    }
}
